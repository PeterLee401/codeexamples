import React from 'react'
import GameSelectableBox from './gameSelectableBox.js'
import renderer from 'react-test-renderer'

describe('GameSelectableBox', () => {
  it('renders correctly', () => {
  const _GameSelectableBox = renderer
    .create(<GameSelectableBox />)
    .toJSON();
  expect(_GameSelectableBox).toMatchSnapshot();
  })

  it('renders red dot', () => {
  const _GameSelectableBox = renderer
    .create(<GameSelectableBox color='red' />)
    .toJSON();
  expect(_GameSelectableBox).toMatchSnapshot();
  })

  it('renders black dot', () => {
  const _GameSelectableBox = renderer
    .create(<GameSelectableBox color='black' />)
    .toJSON();
  expect(_GameSelectableBox).toMatchSnapshot();
  })

  it('renders without dot', () => {
  const _GameSelectableBox = renderer
    .create(<GameSelectableBox />)
    .toJSON();
  expect(_GameSelectableBox).toMatchSnapshot();
  })
})