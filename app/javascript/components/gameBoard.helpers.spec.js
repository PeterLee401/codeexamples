import React from 'react'
import { TopWall, LeftWall, RightWall, BottomWall, Corners } from './gameBoard.helpers'
import renderer from 'react-test-renderer'

describe('GameBoard Helpers', () => {
  it('should render correct constant values', () => {
  expect(TopWall).toMatchSnapshot();
  expect(LeftWall).toMatchSnapshot();
  expect(RightWall).toMatchSnapshot();
  expect(BottomWall).toMatchSnapshot();
  expect(Corners).toMatchSnapshot();
  })
})