import React from 'react'
import { isUndefined, toUpper } from 'lodash'
import GameSelectableBox from './gameSelectableBox.js'
import { TopWall, LeftWall, RightWall, BottomWall, Corners } from './gameBoard.helpers.js'

export class GameBoard extends React.Component {
  constructor(props) {
    super(props)
    this.state = { gridSize: 8, currentUserColor: 'black', 28: 'black', 29: 'red', 36: 'red', 37: 'black' }
  }

  applyFlippingLogic = (id) => {
    const isCorner = Corners[id]
    const isTopWall = TopWall[id]
    const isRightWall = RightWall[id]
    const isBottomWall = BottomWall[id]
    const isLeftWall = LeftWall[id]
    if (isCorner) return this.applyCornerLogic(id)
    if (isTopWall) return this.applyTopWallLogic(id)
    if (isRightWall) return this.applyRightWallLogic(id)
    if (isBottomWall) return this.applyBottomWallLogic(id)
    if (isLeftWall) return this.applyLeftWallLogic(id)
    this.applyNormalLogic(id)
  }

  // this logic isnt really extendable. 4 hour to complete this so
  // lets revisit this if we have time. 
  applyCornerLogic = (id) => {
    // top left
    if (id === 1){
      let Right = this.checkRight(id)
      let BottomRight = this.checkBottomRight(id)
      let Bottom = this.checkBottom(id)
      this.setState({
        ...Right,
        ...BottomRight,
        ...Bottom
      })
    }
    // top right
    if (id === 8) {
      var Left = this.checkLeft(id)
      var BottomLeft = this.checkBottomLeft(id)
      var Bottom = this.checkBottom(id)
      this.setState({
        ...Bottom,
        ...BottomLeft,
        ...Left
      })
    }

    // bottom left
    if (id === 57) {
      var Top = this.checkTop(id)
      var TopRight = this.checkTopRight(id)
      var Right = this.checkRight(id)
      this.setState({
        ...Top,
        ...TopRight,
        ...Right
      })
    }

    // boomt right
    if (id === 64) {
      var Top = this.checkTop(id)
      var TopLeft = this.checkTopLeft(id)
      var Left = this.checkLeft(id)
      this.setState({
        ...Top,
        ...Left,
        ...TopLeft,
      })
    }
  }

  applyTopWallLogic = (id) => {
    console.log('Top Logic')
    var Right = this.checkRight(id)
    var BottomRight = this.checkBottomRight(id)
    var Left = this.checkLeft(id)
    var BottomLeft = this.checkBottomLeft(id)
    var Bottom = this.checkBottom(id)
    this.setState({
      ...Right,
      ...BottomRight,
      ...Bottom,
      ...BottomLeft,
      ...Left,
    })
  }
  applyRightWallLogic = (id) => {
    console.log('Right Logic')
    var Top = this.checkTop(id)
    var TopLeft = this.checkTopLeft(id)
    var Left = this.checkLeft(id)
    var BottomLeft = this.checkBottomLeft(id)
    var Bottom = this.checkBottom(id)
    this.setState({
      ...Top,
      ...Bottom,
      ...BottomLeft,
      ...Left,
      ...TopLeft,
    })
  }
  applyBottomWallLogic = (id) => {
    console.log('Bottom Logic')
    var Top = this.checkTop(id)
    var TopRight = this.checkTopRight(id)
    var Right = this.checkRight(id)
    var TopLeft = this.checkTopLeft(id)
    var Left = this.checkLeft(id)
    this.setState({
      ...Top,
      ...TopRight,
      ...Right,
      ...Left,
      ...TopLeft,
    })

  }
  applyLeftWallLogic = (id) => {
    console.log('Left Logic')
    var Top = this.checkTop(id)
    var TopRight = this.checkTopRight(id)
    var Right = this.checkRight(id)
    var BottomRight = this.checkBottomRight(id)
    var Bottom = this.checkBottom(id)
    this.setState({
      ...Top,
      ...TopRight,
      ...Right,
      ...BottomRight,
      ...Bottom,
    })
  }
  applyNormalLogic = (id) => {
    console.log('Normal Logic')
    var Top = this.checkTop(id)
    var TopRight = this.checkTopRight(id)
    var Right = this.checkRight(id)
    var BottomRight = this.checkBottomRight(id)
    var TopLeft = this.checkTopLeft(id)
    var Left = this.checkLeft(id)
    var BottomLeft = this.checkBottomLeft(id)
    var Bottom = this.checkBottom(id)
    this.setState({
      ...Top,
      ...TopRight,
      ...Right,
      ...BottomRight,
      ...Bottom,
      ...BottomLeft,
      ...Left,
      ...TopLeft,
    })
  }

  checkTop = (currentIndex) => {
    return this.traverseAndUpdate(currentIndex, -8)
  }
  checkTopRight = (currentIndex) => {
    return this.traverseAndUpdate(currentIndex, -7)
  }
  checkRight = (currentIndex) => {
    return this.traverseAndUpdate(currentIndex, 1)
  }
  checkBottomRight = (currentIndex) => {
    return this.traverseAndUpdate(currentIndex, 9)
  }
  checkBottom = (currentIndex) => {
    return this.traverseAndUpdate(currentIndex, 8)
  }
  checkTopLeft = (currentIndex) => {
    return this.traverseAndUpdate(currentIndex, -9)
  }
  checkLeft = (currentIndex) => {
    return this.traverseAndUpdate(currentIndex, -1)
  }
  checkBottomLeft = (currentIndex) => {
    return this.traverseAndUpdate(currentIndex, 7)
  }

  isPartOfaWall = id => {
    if(TopWall[id]){
      return true
    }
    if(RightWall[id]){
      return true
    }
    if(BottomWall[id]){
      return true
    }
    if(LeftWall[id]){
      return true
    }
    if(Corners[id]){
      return true
    }
    return false
  }

  traverseAndUpdate = (currentIndex, direction) => {
    const result = {}
    const currentUserColor = this.state.currentUserColor
    let _currentIndex = currentIndex
    const notACorner = isUndefined(Corners[_currentIndex])
    const notPartOfTheTopWall = isUndefined(TopWall[_currentIndex])

    // we probally could dry this up. 

    while (notACorner || notPartOfTheTopWall) {
      if (_currentIndex < 1) { break }
      result[_currentIndex] = currentUserColor
      let DirectionalBoxIndex = (_currentIndex + direction)
      let CurrentBlock = this.state[_currentIndex]
      const DirectionalBlock = this.state[DirectionalBoxIndex]
      const NextDirectionalBlock = this.state[DirectionalBoxIndex + direction]
      const isNotSameColorAsCurrentUser = DirectionalBlock !== currentUserColor
      const isNotConsectiveColor = DirectionalBlock !== CurrentBlock

      if (DirectionalBlock) {
        if ( isNotSameColorAsCurrentUser && isNotConsectiveColor) {
          if (DirectionalBlock === NextDirectionalBlock) {
            return result
          }
          result[DirectionalBoxIndex] = currentUserColor
        }
        else {
          return result
        }
        _currentIndex = DirectionalBoxIndex
      } else {
        break
      }
    }

    let CurrentBlock = this.state[_currentIndex]
    if (this.isPartOfaWall[_currentIndex]) {
      // when you own last block
      if (CurrentBlock === currentUserColor) {
        result[_currentIndex] = currentUserColor
        return result
      }
      // guess other player has this blocked.
      return {}
    } else {
      return {}
    }
    return result
  }

  updateScore = (id) => {
    this.setState((prevState, props) => ({
      [id]: this.state.currentUserColor,
      currentUserColor: this.state.currentUserColor === 'black'
        ? 'red'
        : 'black'
    }), this.applyFlippingLogic(id))
  }

  currentScore = (color) => {
    var copyOfState = { ...this.state }
    delete copyOfState['currentUserColor']
    return Object.values(copyOfState).filter((val) => val === color).length
  }

  createBoard() {
    const result = []
    for (var i = 1; i < 65; i++) {
      result.push(
        <GameSelectableBox
          key={`selectableOption_${i}`}
          color={this.state[i]}
          id={i}
          onClickCb={(i) => this.updateScore(i)}
        />
      )
    }
    return result
  }

  render() {
    const gameBoardWrapperStyle = {
      height: '100vh',
      backgroundColor: 'white',
      border: '1px solid black',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    }

    const gameBoardGridStyles = {
      display: 'grid',
      gridTemplateColumns: '12% 12% 12% 12% 12% 12% 12% 12%',
      gridTemplateRows: '12% 12% 12% 12% 12% 12% 12% 12%',
      padding: '10px'
    }

    const gameBoardScoreBoard = {
      minWidth: '200px',
      display: 'flex',
      flexDirection: 'column'
    }

    const currentRedScore = this.currentScore('red')
    const currentBlackScore = this.currentScore('black')
    const endOfGame = (currentRedScore + currentBlackScore) > 63

    // can dry this up and make this into smaller components.
    return (
      <div style={gameBoardWrapperStyle}>
        <div style={gameBoardScoreBoard}>
          {endOfGame
            ?
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <span>The game is over ... </span>
              <img src="https://media1.tenor.com/images/2edba1263b5da66f4b5702b8f5b944ac/tenor.gif?itemid=7718591" alt="GAME OVER" style={{ width: '130px', height: '130px' }} />
              <img src="https://media.giphy.com/media/uDBEvFUmCdo3K/giphy.gif" alt="GAME OVER" style={{ width: '130px', height: '130px' }} />
              <span> Final Score </span>
            </div>
            :
            <div>
              <span>On the clock:</span>
              <h2 style={{ color: this.state.currentUserColor }}>{` Team ${toUpper(this.state.currentUserColor)}`}</h2>
              <h4>Current Score </h4>
            </div>
          }
          <span style={{ color: 'red' }}>Red: {currentRedScore}</span>
          <span style={{ color: 'black' }}>Black: {currentBlackScore}</span>
        </div>
        <div style={gameBoardGridStyles}>
          {this.createBoard()}
        </div>
      </div>
    )
  }
}

export default GameBoard