import React from 'react'
import GameBoard from './gameBoard.js'
import { shallow,configure } from 'enzyme'
import renderer from 'react-test-renderer'
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() });

describe('GameBoard', () => {
  it('renders initial state', () => {
  const _GameBoard = renderer
    .create(<GameBoard />)
    .toJSON();
  expect(_GameBoard).toMatchSnapshot();
  })

  describe('should correctly count score', () => {
    it('should return correct score', () => {
      const _GameBoard = shallow(<GameBoard />)
      const inst = _GameBoard.instance()
      inst.setState({11: "black", 12: "red", 18: "red", 22: "black",
       28: "black", 29: "red", 35: "black", 36: "black", 37: "black", 
       47: "black", 50: "red", 51: "black", 62: "red", 
       gridSize: 8, currentUserColor: "red"});

      expect(inst).toMatchSnapshot();
      expect(inst.currentScore('black')).toBe(8);
      expect(inst.currentScore('red')).toBe(5);
    })
  })

  describe('should correctly update score', () => {
    it('should return correct score', () => {
      const _GameBoard = shallow(<GameBoard />)
      const inst = _GameBoard.instance()
      inst.setState({11: "black", 12: "red", 18: "red", 22: "black",
       28: "black",gridSize: 8, currentUserColor: "red"})
      inst.updateScore(37)
      expect(inst.state['37']).toBe('red')
      expect(inst.state['29']).toBe('red')
      expect(inst.state['currentUserColor']).toBe('black')
    })
  })

  describe('isPartOfaWall', () => {
    it('should return true if current chip is on wall', () => {
      const _GameBoard = shallow(<GameBoard />)
      const inst = _GameBoard.instance()
      expect(inst.isPartOfaWall(2)).toBe(true)
      expect(inst.isPartOfaWall(9)).toBe(true)
      expect(inst.isPartOfaWall(16)).toBe(true)
      expect(inst.isPartOfaWall(58)).toBe(true)
      expect(inst.isPartOfaWall(1)).toBe(true)
      expect(inst.isPartOfaWall(10)).toBe(false)
      expect(inst.isPartOfaWall(31)).toBe(false)
    })
  })
})