
export const TopWall = {
  2: true,
  3: true,
  4: true,
  5: true,
  6: true,
  7: true,
}

export const LeftWall = {
  9: true,
  17: true,
  25: true,
  33: true,
  41: true,
  49: true,
  57: true
}

export const RightWall = {
  16: true,
  24: true,
  32: true,
  40: true,
  48: true,
  56: true,
}

export const BottomWall = {
  58: true,
  59: true,
  60: true,
  61: true,
  62: true,
  63: true,
}

export const Corners = {
  1: true, 
  8: true,
  57: true,
  64: true
}