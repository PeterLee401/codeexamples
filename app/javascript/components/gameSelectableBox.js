import React from 'react'
export class GameSelectableBox extends React.Component {
  render(){
    const selectableBoxStyle = { 
      border: '1px solid blue',
      borderRadius: '10px',
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      margin: '10px',
    }
    let boxStyle = {
      borderRadius: '20px',
      minHeight: '25px',
      minWidth: '25px',
      maxHeight: '75px',
      maxWidth: '75px',
      display: 'flex',
      transition:' height 100ms 0ms, opacity 1000ms 500ms'
    }

    const { color, onClickCb, id} = this.props

    boxStyle = this.props.color
      ? {...boxStyle, backgroundColor: color}
      : boxStyle
      
    return(
      <div style={selectableBoxStyle} onClick={
        color 
          ? f => f
          : () => onClickCb(id)
        } 
      >
         <div style={boxStyle}></div>
      </div>
    )
  }
}

export default GameSelectableBox