import React from 'react'
import TextField from 'material-ui/TextField';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FlatButton from 'material-ui/FlatButton';
import LoginBackground from '../../assets/images/loginPage'
import { get } from 'lodash'
import axios from 'axios'

export class LoginForm extends React.Component {
  updateUser = (e, userEmail) => {
    this.setState({user: userEmail})
  }
  updatePassword = (e, userPassword) => {
    this.setState({password: userPassword})
  }
  login = () => {
    const user = this.state.user
    const password = this.state.password
    axios.post('/users/sign_in', {
      user: {
        email: user,
        password: password
      }
    }).then(function (response) {
      // handle success
      console.log('response');
      console.log(response);
    })
    .catch(function (error) {
      // handle error
      console.log('error');
      console.log(error);
    })
  }
  render() {
    const wrapperStyle = {display: 'grid', gridTemplateColumns: '75% auto', gridTemplateRows: 'auto', height: "100vh"}
    const loginDisplayStyle = { backgroundColor: 'black', backgroundImage: `url(${LoginBackground})`, backgroundRepeat: 'no-repeat', backgroundSize: 'cover', opacity: '.56' }
    const loginFormWrapper = { 
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
      width: '100%'
    }
    const loginFormFieldStyles = {
      width: '80%'
    }
    const user = get(this, 'state.user', false)
    const password = get(this, 'state.password', false)
    return(
      <MuiThemeProvider>
        <div style={wrapperStyle}>
          <div style={loginDisplayStyle}></div>
          <div style={loginFormWrapper}>
            <div style={loginFormFieldStyles}>
              <TextField
                hintText="User Name"
                floatingLabelText="user name"
                fullWidth={true}
                onChange={this.updateUser}
              />
              <TextField
                hintText="Password"
                fullWidth={true}
                floatingLabelText="password"
                type='password'
                onChange={this.updatePassword}
              />
            </div>
            <FlatButton label="forgot password?" secondary={true} />
            { user && password 
              ?<FlatButton label="sign in" primary={true} onClick={this.login} />
              :<FlatButton label="sign in" disabled={true} />
            }
          </div>
        </div>
      </MuiThemeProvider>
    )
  }
}

export default LoginForm